function dataResult = extract_features(mode, dataBase, weights)
% data = extract_features(mode, data)
% funkce pro extrakci priznaku z dat

switch mode
    case 'xyp'
        dataResult = dataBase;
        
    case "dt"
        for m = 1 : length(dataBase)
            data = dataBase{m};
            timestamps = data(:, 3); % Assuming timestamps are in column 3
            
            % Find indices of unique timestamps
            [~, unique_indices, ~] = unique(timestamps, 'stable'); % 'stable' preserves the order
            
            % Keep only the rows with unique timestamps
            data = data(unique_indices, :);
            
            dataBase{m} = data;
        end
        dataResult = cell(size(dataBase));
        for m = 1 : length(dataBase)
            
            if exist('weights', 'var') == 1
                weights = weights;
            else
                weights = [ 2.1070    2.0141    1.1660    0.9573    1.4311    3.0821 1];
            end
            
            data = dataBase(m);
            data = data{1};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            % Calculate derivatives (assuming equal spacing between points)
            timeDiff = diff(data(:, 3)); % Calculate time difference
            dx = diff(x_coordinates); % Calculate x derivative
            dy = diff(y_coordinates); % Calculate y derivative
            distances = sqrt(dx.^2 + dy.^2); % Calculate distances
            speed = distances ./ timeDiff;
            
            normalized_speed = speed - min(speed);
            normalized_speed = (normalized_speed) / (max(normalized_speed));
            
            % secondTimeDiff = diff(timeDiff); % Calculate second time difference
            % dxdx = diff(dx)./secondTimeDiff; % Calculate x second derivative
            % dydy = diff(dy)./secondTimeDiff; % Calculate y second derivative
            % thirdTimeDiff = diff(secondTimeDiff); % Calculate third time difference
            % dxdxdx = diff(dxdx)./thirdTimeDiff; % Calculate x third derivative
            % dydydy = diff(dydy)./thirdTimeDiff; % Calculate y third derivative
            
            % Append derivatives to the data
            num_cols_data = size(data, 2);
            
            data(:, num_cols_data+1) = [0; normalized_speed*weights(7)];
            % data(:, num_cols_data+2) = [0; dx]; % Add x derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+3) = [0; dy]; % Add y derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+4) = [0; timeDiff]; % Add x derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+4) = [0; 0; dydy]; % Add y derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+5) = [0; 0; 0; dxdxdx]; % Add x derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+6) = [0; 0; 0; dydydy]; % Add y derivative (padding with 0 at the beginning)
            
            dataResult{m} = data;
        end
    case "cdt"
        for m = 1 : length(dataBase)
            data = dataBase{m};
            timestamps = data(:, 3); % Assuming timestamps are in column 3
            
            % Find indices of unique timestamps
            [~, unique_indices, ~] = unique(timestamps, 'stable'); % 'stable' preserves the order
            
            % Keep only the rows with unique timestamps
            data = data(unique_indices, :);
            
            dataBase{m} = data;
        end
        dataResult = cell(size(dataBase));
        for m = 1 : length(dataBase)
            
            if exist('weights', 'var') == 1
                weights = weights;
            else
                weights = [ 2.1070    2.0141    1.1660    0.9573    1.4311    3.0821 1];
            end
            
            data = dataBase(m);
            data = data{1};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            % Calculate derivatives (assuming equal spacing between points)
            timeDiff = diff(data(:, 3)); % Calculate time difference
            dx = diff(x_coordinates); % Calculate x derivative
            dy = diff(y_coordinates); % Calculate y derivative
            distances = sqrt(dx.^2 + dy.^2); % Calculate distances
            speed = distances ./ timeDiff;
            
            normalized_speed = speed;
            % secondTimeDiff = diff(timeDiff); % Calculate second time difference
            % dxdx = diff(dx)./secondTimeDiff; % Calculate x second derivative
            % dydy = diff(dy)./secondTimeDiff; % Calculate y second derivative
            % thirdTimeDiff = diff(secondTimeDiff); % Calculate third time difference
            % dxdxdx = diff(dxdx)./thirdTimeDiff; % Calculate x third derivative
            % dydydy = diff(dydy)./thirdTimeDiff; % Calculate y third derivative
            
            % Append derivatives to the data
            num_cols_data = size(data, 2);
            
            data(:, num_cols_data+1) = [0; normalized_speed*weights(7)];
            data(:, num_cols_data+2) = [0; dx*weights(8)]; % Add x derivative (padding with 0 at the beginning)
            data(:, num_cols_data+3) = [0; dy*weights(9)]; % Add y derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+4) = [0; timeDiff]; % Add x derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+4) = [0; 0; dydy]; % Add y derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+5) = [0; 0; 0; dxdxdx]; % Add x derivative (padding with 0 at the beginning)
            % data(:, num_cols_data+6) = [0; 0; 0; dydydy]; % Add y derivative (padding with 0 at the beginning)
            
            dataResult{m} = data;
        end
        
        
end
