function sc = score(mode, signatures, model)
% sc = score(mode, signatures, model)
% funkce pocitajici score daneho modelu

switch mode
    case 'GMM'
        sc = zeros(1, numel(signatures)); % ulozime pravdepodobnosti ze dany podpis (z test) odpovida modelu
        for m = 1 : numel(signatures)
            for n = 1 : numel(model)
                p = pdf(model{n}, signatures{m}(:, n)); % vypocet pdf pro kazdy bod podpisu
                sc(m) = sc(m) + sum(log(p)); % zlogaritmujeme a secteme
            end
        end
    case 'DTW'
        tmpsc = zeros(1, numel(signatures)); % ulozime pravdepodobnosti ze dany podpis (z test) odpovida modelu
        for m = 1 : numel(signatures)
            avrg = 0;
            numInModel = numel(model);
            for n = 1 : numel(model)
                modelsize = size(model{n}(1,:));
                for o = 1 : modelsize(2)
                    p = dtw(model{n}(:,o),signatures{m}(:,o)); 
                    % tmpsc(m) = tmpsc(m) + log(p); 
                    avrg =avrg + (p/modelsize(2))/numInModel;
                end
                %  tmpsc(m) = tmpsc(m) + log(p); 
                % avrg =avrg + p/numInModel;
            end
            tmpsc(m) = avrg;
            % display(tmpsc(m));
        end
        sc = tmpsc;
        % meanval = mean(tmpsc);
        % sc = (-1*tmpsc);
        % sc = double(tmpsc > meanval);
        % for i=1 : numel(sc)
        %     sc(i) = sqrt(sc(i));
        % end
end

end
function score = myDTW(vect1, vect2)

D= zeros(length(vect1)+1, length(vect2)+1);
D(1,1) = 0;
D(2:end,1) = inf;
D(1,2:end) = inf;

for i=2:length(vect1)+1
    for j=2:length(vect2)+1
        cost = norm(vect1(i-1)-vect2(j-1));
        D(i,j) = cost + min([D(i-1,j), D(i,j-1), D(i-1,j-1)]);
    end
end

score = D(end,end);
end
