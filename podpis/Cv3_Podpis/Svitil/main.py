import os
import csv

def convert_csv_to_text(input_csv_file, output_text_file):
    # Open the CSV file for reading
    with open(input_csv_file, 'r', newline='') as csvfile:
        csv_reader = csv.reader(csvfile)
        
        # Open the text file for writing
        with open(output_text_file, 'w') as textfile:
            # Iterate through each row in the CSV file
            for row in csv_reader:
                # Write each row as text in the output file
                textfile.write(' '.join(row) + '\n')

# Path to the directory containing CSV files
folder_path = './'  # Replace with your folder path

# Iterate through all files in the directory
for filename in os.listdir(folder_path):
    if filename.endswith('.csv'):
        input_csv_file = os.path.join(folder_path, filename)
        output_text_file = os.path.join(folder_path, os.path.splitext(filename)[0] + '.txt')
        
        # Call the function to convert CSV to text
        convert_csv_to_text(input_csv_file, output_text_file)



def rename_files(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith('.txt'):
            # Split the filename to extract relevant information
            parts = filename.split('_')
            user_info = parts[1].split('.')[0]  # Extract user info (e.g., "svitil")
            signature_type = parts[2]  # Extract signature type (e.g., "Fals1")

            # Initialize new_prefix
            new_prefix = ''

            # Determine the new prefix based on the signature type
            if signature_type.startswith('Orig'):
                new_prefix = 'U' + user_info[4:] + 'S' + signature_type[4:] + '_'
            elif signature_type.startswith('Fals'):
                new_prefix = 'U' + user_info[4:] + 'S' + str(int(signature_type[4:]) + 20) + '_'
            elif signature_type.startswith('Vzor'):
                new_prefix = 'U' + user_info[4:] + 'S' + signature_type[5:] + '_'

            # Create the new filename in the desired format
            new_filename = new_prefix + '_'.join(parts[3:])

            # Rename the file
            os.rename(os.path.join(folder_path, filename), os.path.join(folder_path, new_filename))


# Call the function to rename the files
rename_files(folder_path)
