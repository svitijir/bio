import os

folder_path = '.'  # Replace with the path to your directory

for filename in os.listdir(folder_path):
    parts = filename.split('_')

    if len(parts) >= 3:  # Check if the parts list has enough elements
        number = parts[2].split('.')[0]  # Extract the number from the filename

        if filename.startswith('svitil.pdf_Fals') and int(number) <= 20:
            new_number = int(number) + 20  # Increase the number by 20
            new_name = f"U41S{new_number}.txt"
            os.rename(os.path.join(folder_path, filename), os.path.join(folder_path, new_name))
        else:
            new_name = f"U41S{number}.txt"
            os.rename(os.path.join(folder_path, filename), os.path.join(folder_path, new_name))
    else:
        print(f"Skipping file '{filename}' due to an unexpected filename structure.")
