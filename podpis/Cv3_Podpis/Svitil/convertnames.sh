#!/bin/bash

for file in svitil.pdf_Fals*; do
    number=$(echo "$file" | awk -F '[_.]' '{print $3}')
    if [ "$number" -gt 20 ]; then
        new_name="U41S${number}.txt"
        mv "$file" "$new_name"
    fi
done

