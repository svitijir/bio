function dataBase = preprocess(mode, dataBase, weights)
% data = preprocess(mode, data)
%   vstupy:
%       MODE: typ predzpracovani dat, string
%               'xyp' znamena, ze z dat vytahne pouze polohu a pritlak

switch mode
    case 'xyp'
        for m = 1 : length(dataBase)
            dataBase{m} = dataBase{m}(:, [1, 2, 7]);
        end
    case 'norm_gmm'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            
            % Find minimum and maximum x and y coordinates
            min_x = min(x_coordinates);
            max_x = max(x_coordinates);
            min_y = min(y_coordinates);
            max_y = max(y_coordinates);
            maxSize = max(max_x - min_x, max_y - min_y);
            
            % Normalize x and y coordinates for each line
            normalized_x = (x_coordinates - min_x) / (maxSize);
            normalized_y = (y_coordinates - min_y) / (maxSize);
            normalized_preassure = data(:, 7) - min(data(:, 7));
            normalized_preassure = (normalized_preassure) / max(normalized_preassure);
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x;
            data(:, 2) = normalized_y;
            data(:, 7) = normalized_preassure;
            
            dataBase{m} = data(:, [1 2 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
    case 'cnorm_gmm'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            
            
            % Normalize x and y coordinates for each line
            normalized_x = normalizeData(x_coordinates);
            normalized_y = normalizeData(y_coordinates);
            normalized_preassure = normalizeData(data(:, 7));
            
            mean_x = mean(normalized_x);
            mean_y = mean(normalized_y);
            mean_preassure = mean(normalized_preassure);
            
            std_x = std(normalized_x);
            std_y = std(normalized_y);
            std_preassure = std(normalized_preassure);
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x;
            data(:, 2) = normalized_y;
            data(:, 7) = normalized_preassure;
            
            
            dataBase{m} = data(:, [1 2 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
        
    case 'norm_dtw'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            times = data(:, 3);
            
            % Find minimum and maximum x and y coordinates
            min_x = min(x_coordinates);
            max_x = max(x_coordinates);
            min_y = min(y_coordinates);
            max_y = max(y_coordinates);
            maxSize = max(max_x - min_x, max_y - min_y);
            
            % Normalize x and y coordinates for each line
            normalized_x = (x_coordinates - min_x) / (maxSize);
            normalized_y = (y_coordinates - min_y) / (maxSize);
            firstTime = times(1);
            normalized_time = times - firstTime;
            
            normalized_preassure = data(:, 7) - min(data(:, 7));
            normalized_preassure = (normalized_preassure) / max(normalized_preassure);
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x;
            data(:, 2) = normalized_y;
            data(:, 3) = normalized_time;
            data(:, 7) = normalized_preassure;
            
            dataBase{m} = data(:, [1 2 3 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
    case 'dtw_max'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            times = data(:, 3);
            
            % Find minimum and maximum x and y coordinates
            min_x = min(x_coordinates);
            max_x = max(x_coordinates);
            min_y = min(y_coordinates);
            max_y = max(y_coordinates);
            maxSize = max(max_x - min_x, max_y - min_y);
            
            % Normalize x and y coordinates for each line
            normalized_x = (x_coordinates - min_x) / (maxSize);
            normalized_y = (y_coordinates - min_y) / (maxSize);
            firstTime = times(1);
            normalized_time = (times - firstTime);
            normalized_time = normalized_time / max(normalized_time);
            normalized_preassure = data(:, 7) - min(data(:, 7));
            normalized_preassure = (normalized_preassure) / max(normalized_preassure);
            
            normalized_azimuth = (data(:, 5)) - min(data(:, 5));
            normalized_azimuth = (normalized_azimuth) / max(normalized_azimuth);
            
            normalized_altitude = (data(:, 6)) - min(data(:, 6));
            normalized_altitude = (normalized_altitude) / max(normalized_altitude);
            if exist('weights', 'var') == 1
                weights = weights;
            else
                weights = [ 2.1070    2.0141    1.1660    0.9573    1.4311    3.0821];
            end
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x * weights(1);
            data(:, 2) = normalized_y * weights(2);
            data(:, 3) = normalized_time*weights(3);
            data(:, 5) = normalized_azimuth * weights(4);
            data(:, 6) = normalized_altitude * weights(5);
            data(:, 7) = normalized_preassure * weights(6);
            
            dataBase{m} = data(:, [1 2 3 5 6 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
    case 'gmm_max'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            times = data(:, 3);
            
            % Find minimum and maximum x and y coordinates
            min_x = min(x_coordinates);
            max_x = max(x_coordinates);
            min_y = min(y_coordinates);
            max_y = max(y_coordinates);
            maxSize = max(max_x - min_x, max_y - min_y);
            
            % Normalize x and y coordinates for each line
            normalized_x = (x_coordinates - min_x) / (maxSize);
            normalized_y = (y_coordinates - min_y) / (maxSize);
            firstTime = times(1);
            normalized_time = times - firstTime;
            
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x;
            data(:, 2) = normalized_y;
            data(:, 3) = normalized_time;
            dataBase{m} = data(:, [1 2 3 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
        
        
    case 'dtw_cmax'
        for m = 1 : length(dataBase)
            data = dataBase{m};
            x_coordinates = data(:, 1);
            y_coordinates = data(:, 2);
            times = data(:, 3);
            
            % Find minimum and maximum x and y coordinates
            min_x = min(x_coordinates);
            max_x = max(x_coordinates);
            min_y = min(y_coordinates);
            max_y = max(y_coordinates);
            maxSize = max(max_x - min_x, max_y - min_y);
            
            % Normalize x and y coordinates for each line
            normalized_x = normalizeData(x_coordinates);
            normalized_y = normalizeData(y_coordinates);

            firstTime = times(1);

            normalized_time = times - firstTime;
            normalized_time = normalized_time / max(normalized_time);

            normalized_preassure = normalizeData(data(:, 7));
            
            normalized_azimuth = (data(:, 5)) - min(data(:, 5));
            normalized_azimuth = (normalized_azimuth) / max(normalized_azimuth);
            
            normalized_altitude = (data(:, 6)) - min(data(:, 6));
            normalized_altitude = (normalized_altitude) / max(normalized_altitude);

            if exist('weights', 'var') == 1
                weights = weights;
            else
                weights = [ 1    1    1    1    1    1];
            end
            % Update the x and y coordinates in the original data
            data(:, 1) = normalized_x * weights(1);
            data(:, 2) = normalized_y * weights(2);
            data(:, 3) = normalized_time * weights(3);
            data(:, 5) = normalized_azimuth * weights(4);
            data(:, 6) = normalized_altitude * weights(5);
            data(:, 7) = normalized_preassure * weights(6);
            
            dataBase{m} = data(:, [1 2 5 6 7]);
            % dataBase{m} =[ normalized_x, normalized_y, dataBase{m}(:, [7])];
            % mozne doplnit pridani dalsich priznaku k datum
            %% doplnte
        end
end

    function normalizedData = normalizeData(inputData)
        % Calculate mean and standard deviation of the input data
        dataMean = mean(inputData);
        dataStdDev = std(inputData);
        
        % Normalize the data to have mean 0 and standard deviation 1
        normalizedData = (inputData - dataMean) / dataStdDev;
    end

end
