function [data clss] = load_data(source, input)
    % [data clss] = load_data(source, input)
    %   vstupy:
    %       SOURCE je string oznacujici zdroj dat
    %       INPUT jsou vstupni parametry metody, zadavaji se jako struktura
    %               INPUT.id je id subjektu, ktereho podpisy chceme nacist
    %   
    %   vystupy:
    %       DATA je cell array s jednotlivymi nactenymi daty v raw formatu
    %       CLSS je zarazeni do class, pole stejne delky jako vstupni DATA s 0
    %       oznacujucu v datech pozici originalu a 1 oznacujuci pozici
    %       falsifikatu
        
    
    switch source
        case 'SVC'
            fpath = './SVC2004';
            id = input.id;
            clss = [zeros(20, 1); ones(20, 1)];
        case 'OWN'
            fpath = './OWN';
            id = input.id;
            clss = [zeros(8,1); ones(5,1)];
    end
    
    data = cell(length(clss), 1);
    
    switch source
        case 'SVC'
            for m = 1 : length(data)
                txtfile = sprintf('U%dS%d.TXT', id, m);
                fname = fullfile(fpath,txtfile);
                %fid = fopen(fname);
                fid = fileread(fname);
    
                % nacteni dat
                mat = textscan(fid,'%f %f %f %f %f %f %f','Delimiter',' ','HeaderLines',1);
    
    
                data{m} = cell2mat(mat);
            end
        case 'OWN'
            vz = 0; ori = 0; fal = 0;
            for m = 1 : length(data)
                if m <= 3
                    vz = vz+1;
                    csvfile = sprintf('DP_BIO_Test_sidlova.pdf_Vzor%d_%03d.csv', vz, m);
                elseif m <= 8
                    ori = ori+1;
                    csvfile = sprintf('DP_BIO_Test_sidlova.pdf_Orig%d_%03d.csv', ori, m);
                else
                    fal = fal+1;
                    csvfile = sprintf('DP_BIO_Test_sidlova.pdf_Fals%d_%03d.csv', fal, m);
                end
                
                fname = fullfile(fpath,csvfile);
                fid = fileread(fname);
    
                % nacteni dat a prevedeni do stejneho formatu
                mat = textscan(fid,'%f %f %f %f','Delimiter',',','HeaderLines',0);
                mat2 = nan(length(mat{1}),7);
                mat2(:,[1, 2, 7, 3]) = cell2mat(mat);
    
                data{m} = mat2;
            end
    end
    
    
    
    
    