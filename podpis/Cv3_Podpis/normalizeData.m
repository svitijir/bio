function normalizedData = normalizeData(inputData)
    % Calculate mean and standard deviation of the input data
    dataMean = mean(inputData);
    dataStdDev = std(inputData);
    
    % Normalize the data to have mean 0 and standard deviation 1
    normalizedData = (inputData - dataMean) / dataStdDev;
    end