


vstupni_data = 'SVC';
numOfPeople = 40;
acctualNumberOfPeople = 0;
average = 0;

method = "DTW";
preprocessing = "dtw_cmax";
featureExtraction = "xyp";

multiscores = zeros(1,0);
multiClss = zeros(0, 1);
dataBaseOfPeople = {};
weights = [  1.0284    0.9875    1.0393    1.0651    0.8829    1.0602    0.9951    1.0354    1.0336 ];
errors = 0;
for i = 1:numOfPeople
        subjekt.id = i;
        [data clss] = load_data(vstupni_data, subjekt); %nacteni vstupnich dat
        
        %ukazka podpisu a padelku
        % figure; subplot(121)
        % plot(data{1}(:, 1), data{1}(:, 2)); title('pravy podpis');
        % subplot(122)
        % plot(data{21}(:, 1), data{21}(:, 2)); title('padelek')
        
        %data = preprocess('xyp', data); %preprocess
        data = preprocess(preprocessing, data, weights);
        data = extract_features(featureExtraction, data, weights); %extrakce priznaku, zatim nic
        % nedela
        
        lNum = 3; %pocet podpisu, ktere budou pozity na tvorbu modelu
        input.data = data;
        input.learnNum = lNum;
        input.compNum = 5; %pocet gausianu pouzitych pro GMM
        model = make_model(method, input); %tvorba modelu
        
        testSignatures = data(lNum+1:end); %testovaci data budou ta, ze kterych nebyl model tvoren
        
        %vykresleni vyslednych score pro obe skupiny podpisu, mozne pomoci
        %boxplotu, nebo histogramu, pripadne ROC krivky
        % figure;
        scores = score(method, testSignatures, model);
        multiscores = cat(2, multiscores, scores);
        multiClss = cat(1,multiClss, clss(lNum+1:end));
        dataBaseOfPeople{end+1} = {scores, clss(lNum+1:end)};
        
        % boxplot(scores, clss(lNum+1:end))
        % xlabel('tridy, 0 = prave podpisy, 1 = falsifikaty');
        % ylabel('hodnota score = mira podobnosti podpisu s modelem');
        
        %ROC krivka
        % plotroc(abs(clss(lNum+1:end)'-1), -scores)
        %vykresleni pomoci histogramu
        % figure
        % hist(scores(clss(lNum+1:end)==0))
        % hold on
        % [b, bs] = hist(scores(clss(lNum+1:end)==1));
        % bar(bs, b, 'r')
        % xlabel('score = mira podobnosti s modelem'); ylabel('cetnost')
        % legend({'prave podpisy', 'falsifikaty'})
        
        %vypocet chyby EER
        if method == "GMM"
            [eer FP FR thresh] = get_eer(clss(lNum+1:end), scores);
        else
            [eer FP FR thresh] = get_eer_dtw(clss(lNum+1:end), scores);
        end
        average = average + median(thresh);
        acctualNumberOfPeople = acctualNumberOfPeople+1;
        
        % display(["treshold: " thresh " FP: " FP " FR: " FR " EER: " eer]);
        % figure; plot(thresh, [FP' FR']);
        % xlabel('score'); ylabel('pomerna chyba')
        % legend({'false positive rate', 'false negative rate'});
        

end
numOfPeople = numOfPeople - errors;

if method == "GMM"
    [eer FP FR thresh] =  get_eer(multiClss, multiscores);
    
    display(["treshold: " median(thresh) " FP: " median(FP) " FR: " median(FR) " EER: " median(eer)]);
    figure; plot(thresh, [FP' FR']);
    xlabel('score'); ylabel('pomerna chyba')
    legend({'false positive rate', 'false negative rate'});
    figure

    plotroc(abs(multiClss'-1), multiscores);

else
    [eer FP FR thresh] = get_eer_dtw(multiClss, multiscores);
    
    display(["treshold: " median(thresh) " FP: " median(FP) " FR: " median(FR) " EER: " median(eer)]);
    figure; plot(thresh, [FP' FR']);
    xlabel('score'); ylabel('pomerna chyba')
    legend({'false positive rate', 'false negative rate'});
    figure

    plotroc(abs(multiClss'-1), -multiscores);

    
end


minSum = Inf;
minIndex = 0;
for i = 1:length(FP)
    if(abs(FP(i) - FR(i)) < minSum)
        minSum = abs(FP(i) - FR(i));
        minIndex = i;
    end
end




average = thresh(minIndex);
display(["prumerna chyba: " average]);
%  average = -7000;
FRR = 0;
FAR = 0;

totalNumberOfTrueSignatures = 0;
totalNumberOfFalseSignatures = 0;
for i = 1:numOfPeople
    scores = dataBaseOfPeople{i}{1};
    clss = dataBaseOfPeople{i}{2};
    
    for j = 1:length(scores)
        
        if(scores(j) > average)
            if(clss(j) == 0)
                FRR = FRR + 1;
            end
            totalNumberOfTrueSignatures = totalNumberOfTrueSignatures + 1;
            
        else
            if(clss(j) == 1)
                FAR = FAR + 1;
            end
            totalNumberOfFalseSignatures = totalNumberOfFalseSignatures + 1;
            
        end
    end
    
end

FRR = FRR/totalNumberOfTrueSignatures;
FAR = FAR/totalNumberOfFalseSignatures;

display(["FRR: " FRR*100 " FAR: " FAR*100]);