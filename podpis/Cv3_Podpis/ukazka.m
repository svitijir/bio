%ukazkovy skript
vstupni_data = 'OWN';
subjekt.id = 40;
[data clss] = load_data(vstupni_data, subjekt); %nacteni vstupnich dat
modelType = 'DTW'; %typ modelu, ktery chceme vytvorit
%ukazka podpisu a padelku
figure; subplot(121)
plot(data{11}(:, 1), data{11}(:, 2)); title('pravy podpis');
subplot(122)
plot(data{2}(:, 1), data{2}(:, 2)); title('padelek')

%data = preprocess('xyp', data); %preprocess
weights = [  1.0284    0.9875    1.0393    1.0651    0.8829    1.0602    0.9951    1.0354    1.0336 ];
data = preprocess('xyp', data, weights);
data = extract_features('xyp', data, weights); %extrakce priznaku, zatim nic
% nedela

lNum = 5; %pocet podpisu, ktere budou pozity na tvorbu modelu
input.data = data;
input.learnNum = lNum;
input.compNum = 5; %pocet gausianu pouzitych pro GMM
model = make_model(modelType, input); %tvorba modelu

testSignatures = data(lNum+1:end); %testovaci data budou ta, ze kterych nebyl model tvoren

%vykresleni vyslednych score pro obe skupiny podpisu, mozne pomoci
%boxplotu, nebo histogramu, pripadne ROC krivky
figure; 
scores = score(modelType, testSignatures, model);
boxplot(scores, clss(lNum+1:end))
xlabel('tridy, 0 = prave podpisy, 1 = falsifikaty');
ylabel('hodnota score = mira podobnosti podpisu s modelem');

%ROC krivka
plotroc(abs(clss(lNum+1:end)'-1), -scores)
%vykresleni pomoci histogramu
figure
hist(scores(clss(lNum+1:end)==0))
hold on
[b, bs] = hist(scores(clss(lNum+1:end)==1));
bar(bs, b, 'r')
xlabel('score = mira podobnosti s modelem'); ylabel('cetnost')
legend({'prave podpisy', 'falsifikaty'})

%vypocet chyby EER
[eer FP FR thresh] = get_eer_dtw(clss(lNum+1:end), scores);
display(["treshold: " median(thresh) " FP: " median(FP) " FR: " median(FR) " EER: " median(eer)]);
figure; plot(thresh, [FP' FR']);
xlabel('score'); ylabel('pomerna chyba')
legend({'false positive rate', 'false negative rate'});