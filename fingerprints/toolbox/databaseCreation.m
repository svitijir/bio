addpath ./predzpracovani
addpath ./porovnani
clear all; clc


% nacteni obrazku otisku
%id = 1; num = 6;
%[im1 name] = loadfprint(id, num, 'DataB');
% [imSegmented imContour] = segmentimage(im1, 9); % pro overeni segmentace
% show_segmentation(im1, imContour);

%id = 1; num = 5;
%[im2 name2] = loadfprint(id, num, 'DataB');
% [imSegmented imContour] = segmentimage(im2, 9); % pro overeni segmentace
% show_segmentation(im2, imContour);
%imS1 = preprocess(im1, name, 9);

%imS2 = preprocess(im2, name2, 9);



files = dir('../data/DataBase/*.bmp');
fingerprints = cell(0, 2); % Each row will contain a pair (name, vector)
for file = files'
     matches = regexp(file.name, '\d+', 'match');
     numbers = str2double(matches);
     if numel(numbers) == 2
          try
               id = numbers(1);
               num = numbers(2);
               [im1 name] = loadfprint(id, num, 'DataB');
               imS1 = preprocess(im1, file.name, 9);
               for i = 1:size(imS1.singularityPoints, 1)
                    vector = generate_fingercode(imS1.im, imS1.singularityPoints(i, :));
                    fingerprints{end + 1, 1} = file.name; % Add name to the first column
                    fingerprints{end, 2} = vector;  % Add vector to the second column
               end
          catch exception
               disp(['cannot proces' file.name]);
               disp(exception);
          end
     end
     % Do some stuff
end

save('fingerprint_data.mat', 'fingerprints');