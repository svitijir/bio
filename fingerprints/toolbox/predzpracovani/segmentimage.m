function [imSegmented, imContour] = segmentimage(imOriginal)

%[imSegmented imContour] = segmentimage(imOriginal, blkSize, enh, smooth)
%
% Funkce segmentuje obrazek na otisk a pozadi.
%
%   imOriginal - vstupni otisk
%   tresh - prah pro segmentaci otisku
%
%
% vystupy:
%   imSegmented - binarni obrazek stejne velikosti jako imOriginal, 1
%       oznacuje otisk a 0 znaci pozadi
%   imContour - kontura segmentace binarni obrazek obsahujici pouze
%   konturu, ne celou plochu segmentace


N = 6;

imOriginal = normalizeImage(imOriginal);
%[Gfrg, Vfrg] = calculateFrgValues(imOriginal, N);
means = calculateMean(imOriginal, N);
variances = calculateVariance(imOriginal, N);
coherence = calculateCoherence(imOriginal, N);
windowSize = 3;  % Adjust this value as needed; it defines the size of the local neighborhood
variances = medfilt2(variances, [windowSize, windowSize]);
coherence = medfilt2(coherence, [windowSize, windowSize]);

%imshow(variances); % Display the second image in the second figure

[height, width] = size(imOriginal);

resultImage = zeros(height, width);

for i = 1:N:height
    for j = 1:N:width
        x1 = j;
        x2 = min(j + N - 1, width);
        y1 = i;
        y2 = min(i + N - 1, height);
        if linearClassification(means(ceil(i/N), ceil(j/N)), variances(ceil(i/N), ceil(j/N)), coherence(ceil(i/N), ceil(j/N)))
            for k = x1:x2
                for l = y1:y2
                    resultImage(l, k) = 1;
                end
            end
        end
    end
end

% Find connected components in the segmented image
cc = bwconncomp(resultImage);

% Identify the largest labeled area
numPixels = cellfun(@numel, cc.PixelIdxList);
[~, idx] = max(numPixels);

% Create a binary image containing only the largest labeled area
largestArea = zeros(size(resultImage));
largestArea(cc.PixelIdxList{idx}) = 1;

% Fill holes inside the largest connected component
filledLargestArea = imfill(largestArea, 'holes');

% Set all other components to background (0)
imSegmented = filledLargestArea == 1;
[imSegmented, imContour] = removeNonRoundComponents(imSegmented);



% The following lines appear to be placeholders or comments and are not functional code.
% If you need to add additional code, do so here.
end

function [resultSegmented] = fitEllipse(oneComponentImage)
% Calculate the region properties using regionprops
% Calculate centroid, orientation and major/minor axis length of the ellipse
s = regionprops(oneComponentImage,{'Centroid','Orientation','MajorAxisLength','MinorAxisLength'});
% Calculate the ellipse line
theta = linspace(0,2*pi);
col = (s.MajorAxisLength/2)*cos(theta);
row = (s.MinorAxisLength/2)*sin(theta);
M = makehgtform('translate',[s.Centroid, 0],'zrotate',deg2rad(-1*s.Orientation));
D = M*[col;row;zeros(1,numel(row));ones(1,numel(row))];

binaryImage = false(size(oneComponentImage));
% Convert contour points to integer coordinates
D = round(D);

% Ensure the contour points are within the image bounds
D(1, D(1, :) < 1) = 1;
D(1, D(1, :) > size(oneComponentImage, 2)) = size(oneComponentImage, 2);
D(2, D(2, :) < 1) = 1;
D(2, D(2, :) > size(oneComponentImage, 1)) = size(oneComponentImage, 1);

% Create a binary mask by filling the interior of the ellipse
mask = poly2mask(D(1, :), D(2, :), size(oneComponentImage, 1), size(oneComponentImage, 2));
binaryImage(mask) = true;

% Use the ellipse mask to segment the image
resultSegmented = binaryImage ;%& ellipseMask;
end

% Define the function to remove non-round components
function [resultSegmented, imContour] = removeNonRoundComponents(imSegmented)
% Calculate region properties
% Fit an ellipse to the largest connected component
imageCropped = imSegmented;
se_size = 14; % Adjust as needed

for i = 1:1:se_size
    cutContour = bwperim(imageCropped);
    imageCropped = imageCropped - cutContour;
    % figure;
    % imshow(imageCropped);
    cc = bwconncomp(imageCropped);
    largestArea = zeros(size(imSegmented));
    % Identify the largest labeled area
    numPixels = cellfun(@numel, cc.PixelIdxList);
    [~, idx] = max(numPixels);
    largestArea(cc.PixelIdxList{idx}) = 1;
    imageCropped = largestArea;
    
end
% resultSegmented = imageCropped;
resultSegmented = fitEllipse(imageCropped);% & imdilate(imSegmented, strel('disk',round(se_size/4)));
resultSegmented = imfill(resultSegmented, 'holes');

% Erode the binary component
%resultSegmented = imerode(resultSegmented, strel('disk', se_size));

% Dilate the eroded component
resultSegmented = imdilate(resultSegmented, strel('disk', se_size));

% Extract the boundary of the modified image
imContour = bwperim(resultSegmented);
end

function [pass] = linearClassification(mean, variance, coherence)
neuron = [0.0129, -0.07, 0.99, 0.015];
input = [coherence, mean, variance, 1];
pass = neuron * input' > 0;
end

function [means] = calculateMean(imOriginal, N)
[height, width] = size(imOriginal);

blokHeight = ceil(height/N);
blokWidth = ceil(width/N);
means = zeros(blokHeight, blokWidth);

% Calculate means for subimages
for i = 1:N:height
    for j = 1:N:width
        % Define the coordinates of the current patch
        x1 = j;
        x2 = min(j + N - 1, width);
        y1 = i;
        y2 = min(i + N - 1, height);
        
        % Extract the patch
        patch = imOriginal(y1:y2, x1:x2);
        
        % Calculate the mean of the patch
        mean_value = mean(patch(:));
        
        % Store the mean value in the 2D array
        row = ceil(i/N);
        col = ceil(j/N);
        means(row, col) = mean_value;
    end
end

end

function [coherence] = calculateCoherence(imOriginal, N)
[Gx, Gy] = gradient(imOriginal);
[height, width] = size(imOriginal);

blokHeight = ceil(height/N);
blokWidth = ceil(width/N);
coherence = zeros(blokHeight, blokWidth);

for i = 1:N:height
    for j = 1:N:width
        x1 = j;
        x2 = min(j + N - 1, width);
        y1 = i;
        y2 = min(i + N - 1, height);
        
        Gx_patch = Gx(y1:y2, x1:x2);
        
        GxSquared = Gx_patch .* Gx_patch;  % Calculate Gx^2
        Gxx = sum(GxSquared(:)); % Sum all values in Gx^2
        
        Gy_patch = Gy(y1:y2, x1:x2);
        GySquared = Gy_patch .* Gy_patch;  % Calculate Gx^2
        Gyy = sum(GySquared(:)); % Sum all values in Gx^2
        
        GxySquared = Gx_patch .* Gy_patch; % Sum all values in Gx * Gy
        Gxy = sum(GxySquared(:)); % Sum all values in Gx * Gy
        
        coherence_value = sqrt((Gxx - Gyy)^2 + 4 * Gxy^2) / (Gxx + Gyy);
        
        row = ceil(i/N);
        col = ceil(j/N);
        coherence(row, col) = coherence_value;
    end
end

end

function [variances] = calculateVariance(imOriginal, N)
[height, width] = size(imOriginal);

blokHeight = ceil(height/N);
blokWidth = ceil(width/N);
variances = zeros(blokHeight, blokWidth);

for i = 1:N:height
    for j = 1:N:width
        x1 = j;
        x2 = min(j + N - 1, width);
        y1 = i;
        y2 = min(i + N - 1, height);
        
        patch = imOriginal(y1:y2, x1:x2);
        variance_value = var(patch(:));
        
        row = ceil(i/N);
        col = ceil(j/N);
        variances(row, col) = variance_value;
    end
end

end

function [Gfrg, Vfrg] = calculateFrgValues(imOriginal, N)
GfrgCount = 0;
VfrgCount = 0;
MEAN = mean(imOriginal(:));
VAR = var(imOriginal(:));
[height, width] = size(imOriginal);

Gfrg = 0;
Vfrg = 0;
for i = 1:N:height
    for j = 1:N:width
        x1 = j;
        x2 = min(j + N - 1, width);
        y1 = i;
        y2 = min(i + N - 1, height);
        patch = imOriginal(y1:y2, x1:x2);
        mean_value = mean(patch(:));
        var_value = var(patch(:));
        if(mean_value > MEAN)
            GfrgCount = GfrgCount + 1;
            Gfrg = Gfrg + mean_value;
        end
        if(var_value > VAR)
            VfrgCount = VfrgCount + 1;
            Vfrg = Vfrg + var_value;
        end
    end
end
Gfrg = Gfrg / GfrgCount;
Vfrg = Vfrg / VfrgCount;
end


function [normalizedImage] = normalizeImage(imOriginal)
[height, width] = size(imOriginal);

VAR0 = 150;
MEAN0 = 150;
MEAN = mean(imOriginal(:));
VAR = var(imOriginal(:));
normalizedImage = zeros(size(imOriginal));
for i = 1:height
    for j = 1:width
        
        if(imOriginal(i, j) > MEAN)
            normalizedImage(i, j) = MEAN0 + sqrt((VAR0 .* ((imOriginal(i, j) - MEAN).^2)) / VAR);
        else
            normalizedImage(i,j) = MEAN0 - sqrt((VAR0 .* ((imOriginal(i, j) - MEAN).^2)) / VAR);
        end
    end
end

end