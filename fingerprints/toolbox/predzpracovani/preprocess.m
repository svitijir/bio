function [imS, kernel_exists] = preprocess(im1, name, segTreshold, orientationWinSize)

% Vraci strukturu zakldnich priznaku extrahovanou z obrazku.
%
%   imS = preprocess(im1, name, segWinSize, orientationWinSize)
%
%       vstupy: im1 - vstupni obrazek
%               name - nazev
%               segTrehold - velikost okna pro segmentaci otisku (default 3)
%               orientationWinSize - velikost okna pro pocitani
%                  orientacniho pole (default 15)
%
%       vystupy: imS - struktura obsahujici im, name, segmentation,
%           contour, orientedArray, singularityPoints, localFrequencies,
%           imageGaborReconstruct, imageSkeleton, minutiaArray, minutiaRotation
kernel_exists = true;
d = containers.Map();
d("1_12.bmp") = [344, 125];
d("100 (1).bmp") = [359, 135];
d("1_13.bmp") = [359, 135];
d("1_14.bmp") = [89, 332];
d("1_15.bmp") = [350, 150];
d("100 (2).bmp") = [89, 332];

if ~exist('name', 'var'), name = []; end
if isempty(name), name = ''; end
if ~exist('segTreshold', 'var'), segTreshold = []; end
if isempty(segTreshold), segTreshold = 3; end
if ~exist('orientationWinSize', 'var'), orientationWinSize = []; end
if isempty(orientationWinSize), orientationWinSize = 10; end

[im1S im1C] = segmentimage(im1);
oAi1 = computeorientationarray(im1, im1S, orientationWinSize);
sPi1 = findsingularitypoint2(im1, im1S);

if length(sPi1) == 0
    disp(name);
    disp('singularity not found');
    [row, col] = find(im1S == 1);

    kernel_exists = false;
    if d.isKey(name)

        sPi1 = d(name);
    else
        centroidRow = mean(row);
        centroidCol = mean(col);
        sPi1 = [centroidRow, centroidCol];
    end
end



fAi1 = computelocalfrequency(im1, im1S, oAi1);
[im1R, ~, im1Sk] = enhance2ridgevalley(im1, im1S, oAi1, fAi1);
im1Sk = cleanskeleton(im1, im1S, im1C, im1Sk, oAi1);
[mAi1 mRi1] = findminutia(im1Sk, im1C, oAi1);

imS.im = im1;
imS.name = name;
imS.segmentation = im1S;
imS.contour = im1C;
imS.orientedArray = oAi1;
imS.singularityPoints = sPi1;
imS.localFrequencies = fAi1;
imS.imageGaborReconstruct = im1R;
imS.imageSkeleton = im1Sk;
imS.minutiaArray = mAi1;
imS.minutiaRotation = mRi1;