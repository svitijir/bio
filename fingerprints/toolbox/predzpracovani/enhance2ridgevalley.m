function [imReconstruct imBinary imSkeleton] = enhance2ridgevalley(imOriginal, imSegmented, orientationArray, frequencyArray, filtoff)

%[imReconstruct imBinary imSkeleton] = enhance2ridgevalley(imOriginal,
%   imSegmented, orientationArray, frequencyArray, filtoff)
%
% Funkce implementuje smerovou filtraci pomoci Gaborovych filtru.
% Maximalni pocet filtru : 48
% Smerovy rozsah : [- pi/2 : pi/8 : pi/2]
% Maximalne 3 frekvence
% Paramter Sigma {2, 4)
%
% Fuknce provani vylepseni kvality otisku prstu pomoci filtrace Gaborovymi
% filtra a zaroven provadi skeletonizaci vysledneho obrazku.
%
%   imOriginal - vstupni obrazek otisku
%   imSegmented - segmentace otisku
%   orientationArray - orientacni pole
%   frequencyArray - pole lokalni frekvence papilarnich linii
%   filtoff - true = vypnuti G. filtrace, false = zapnuti filtrace


if nargin < 5, filtoff = false; end

Filteredimages = cell(2,8,3);
%Reconstructedimages = cell(2,3);
% binaryBlkSize = 20;
binaryBlkSize = 32;

imoriginal = imOriginal;
width = size(imoriginal, 2);
height = size(imoriginal, 1);

im = imoriginal;

% if ~filtoff
% determines 3 freqences that will be used for directional filtering
frequencyVector = choosefrequency(frequencyArray(1, :));


% creation of set of gabor filters. Gebor parameters sigma are determined
% by variable indsi, frequency fiven by Fp.frequencyVector, orientation
% given by indan (8 orientations)
for indfr = 1 : length(frequencyVector)
    frequency = frequencyVector(indfr);
    indsi = 2;
    sigmax = 2 * indsi;
    sigmay = 2 * indsi;
    
    for indan = -4 : 3
        angle = indan * (pi/8);
        imFiltered = filtergabor(im, angle, frequency, sigmax, sigmay);
        Filteredimages{indsi, indan + 5, indfr} = imFiltered;   %cell array of resulting Gabor-filtered images
    end
end


% construct final image. For every pixel it takes the corresponding values from Fp.frequencyArray and Fp.orientationArray
% and rounds them to the closest frequency values of Gabor fiters (from
% Fp.frequencyVector) and finds the closest discrete angular value (orientations)
%
% contourDist = bwdist(imContour);
% contourDist = imresize(contourDist, .3);
% contour = imSegmented;


% [x y]=find(frequencyArray);
% xm=min(x);
% xM=max(x);
frLen = size(frequencyArray, 2);
for i = 1 : size(im,1)
    for j = 1 : size(im,2)
        if imSegmented(i,j)~=0
            %                 if (i<xm) || (i>xM)
            %                     imOutput(i,j)=1;
            %                 else
            % chose appropriate frequence
            distances = sqrt(sum((frequencyArray(2:3, :) - repmat([i;j], 1, frLen)).^2));
            minDist = distances == min(distances);
            frq = frequencyArray(1, minDist);
            temp(1 : length(frequencyVector)) = frq(1);
            temp = abs(frequencyVector - temp);
            freqIndice = find(temp == min(temp));
            
            ang = orientationArray(i,j);
            ang = - round(ang/(pi/8));   %finds the closest discrete angular value (orientations)
            if ang == 4; ang = -4; end
            
            if filtoff
                imOutput(i,j) = imOriginal(i,j);
            else
                imOutput(i,j) = Filteredimages{2, ang + 5,freqIndice(1)}(i,j);
            end
            %                 end
            
        else
            imOutput(i,j) = 0;
        end
        
    end
end
% else
%     imOutput = im;
% end
display(size(imSegmented));
nanImg = nan(size(imSegmented)); 
nanImg(imSegmented) = 1;
imReconstruct = mat2gray(imOutput) .* nanImg;    %reconstructed image
imReconstruct = blkpad(imReconstruct, binaryBlkSize);
warning('off', 'Images:BLKPROC:DeprecatedFunction');
imReconstruct = blkproc(imReconstruct, [binaryBlkSize binaryBlkSize], @binarizeimage);
warning('on', 'Images:BLKPROC:DeprecatedFunction');
imReconstruct = imcrop(imReconstruct, [0 0 width height]);

imBinary = imReconstruct;            %binary version of imReconstruct
imOutput = bwmorph(imcomplement(imReconstruct),'thin', 'Inf'); %thins the reconstructed image
imSkeleton = imOutput;               %stores the thinned binary image in Fp.imSkeleton

end


%--------------------------------------------------------------------------
%Sub-Function filtergabor
%--------------------------------------------------------------------------
function imfilt = filtergabor(im, angle_rad, frequency, sigmax, sigmay)
    x = -16:16;
    y = -16:16;
    
    % Convert angle to radians

    % Create a meshgrid for x and y
    [X, Y] = meshgrid(x, y);
    % Rotate the coordinates to the specified angle
    X_rot = sin(angle_rad) * X + cos(angle_rad) * Y;
    Y_rot = sin(angle_rad) * Y - cos(angle_rad) * X ;

    % Create the Gabor kernel in the frequency domain
    %gabor_kernel = exp(...
    %    -(X_rot.^2 / (2 * sigmax^2) + Y_rot.^2 / (2 * sigmay^2)) ...
    %    .* cos(2 * pi * frequency * X_rot));
    gabor_kernel = exp(...
        (-0.5)*((X_rot.^2) / (sigmax^2) + (Y_rot.^2) / (sigmay^2))) ...
        .* cos(2 * pi * frequency * X_rot);
    
    % Apply a window function to reduce boundary effects (e.g., Hamming window)
    [m, n] = size(gabor_kernel);
    hamming_window = hamming(m) * hamming(n).';
    gabor_kernel = gabor_kernel .* hamming_window;

    % Perform convolution with the input image
    imfilt = conv2(im, gabor_kernel, 'same');

    % Normalize the filtered image to the range [0, 1]
    imfilt = mat2gray(imfilt);
end


%--------------------------------------------------------------------------
%Sub-Function binarizeimage
%--------------------------------------------------------------------------
function  Iout = binarizeimage(Iin)
level = graythresh(Iin(~isnan(Iin)));
Iout = im2bw(Iin, level);

end