addpath ./predzpracovani
addpath ./porovnani
clear all; clc

id = 4; num = 13;
[im1 name] = loadfprint(id, num, 'moje');
[imS1,kernel_exists] = preprocess(im1, name, 9);


loaded_data = load('fingerprint_data.mat');
loaded_fingerprints = loaded_data.fingerprints;


results = {};
for i = 1:size(imS1.singularityPoints, 1)
    vector = generate_fingercode(imS1.im, imS1.singularityPoints(i, :));
    minDifferenceVectorScalar = Inf;
    minName = "nothing found";
    for fingerprint = loaded_fingerprints'
        fingerprintVector = fingerprint{2};
        D = abs(vector - fingerprintVector);
        matchingScoreG = mean(D);
        if(matchingScoreG < minDifferenceVectorScalar)
            minDifferenceVectorScalar = matchingScoreG;
            minName = fingerprint{1};
        end
    end
    disp(minName);

    results{end+1} = {minDifferenceVectorScalar, minName};
end

minNameCounts = containers.Map('KeyType', 'char', 'ValueType', 'double');

for i = 1:numel(results)
    weight = 1;
    if kernel_exists && i == 1
        weight = 3;
    end
    minName = results{i}{2};
    if isKey(minNameCounts, minName)
        minNameCounts(minName) = minNameCounts(minName) + 1;
    else
        minNameCounts(minName) = 1;
    end
end

% Find the most common minName
mostCommonMinName = '';
maxCount = 0;
minNames = keys(minNameCounts);
for i = 1:numel(minNames)
    currentCount = minNameCounts(minNames{i});
    if currentCount > maxCount
        mostCommonMinName = minNames{i};
        maxCount = currentCount;
    end
end

% Display the most common minName and its count
disp(['Most common minName: ', mostCommonMinName]);
disp(['Count: ', num2str(maxCount)]);
disp(['score: ', num2str(results{1}{1})]);