function [vector] = generate_fingercode(im1, singularityPoints1)
Gfilt = GaborFilter_creation;

vector = fingercode_creation(im1, Gfilt, singularityPoints1, 16, [6,2]);
end