function matchongScoreG = fingercode_score(im1, singularityPoints1, im2, singularityPoints2)
% Spocita matching skore pro dvojici obrazku.
%
% matchongScoreG = fingercode_score(im1, singularityPoints1, im2,
%   singularityPoints2)
%
%   Vstupy: im1 - obrazek 1
%           singularityPoints1 - pole se singularnimi body k otisku 1
%           im2 - obrazek 2
%           singularityPoints2 - pole se singularnimi body k otisku 2

Gfilt = GaborFilter_creation;

fingercode1 = fingercode_creation(im1, Gfilt, singularityPoints1);
fingercode2 = fingercode_creation(im2, Gfilt, singularityPoints2);
disp(size(fingercode1));
D = abs(fingercode1 - fingercode2);
matchongScoreG = mean(D);