function [fingercodeResult Fmasked] = fingercode_creation(imOriginal, Gfilt, core, maskSize, dia)

% Funkce pro vypocet FingerCode priznaku z otisku prstu.
%
% [fingercode Fmasked] = fingercode_creation(imOriginal, Gfilt, core, maskSize, dia)
%
%   vstupy: imOriginal - vstupni obrazek otisku
%           Gfilt - banka Gaborovych filtru
%           core - [x;y] souradnice jadra otisku
%           maskSize - velikost bloku pri blokovem zpracovani (deafult 16)
%           dia - velikost [vnejsi vnitrni] polomer masky
%
%   vystupy: fingercode - priznakovy vektor pro obrazek
%            Fmasked - ilustracni blokovy obrazek s vyrizlym mezikruzim

if ~exist('maskSize', 'var'), maskSize = []; end
if isempty(maskSize), maskSize = 16; end
if ~exist('dia', 'var'), dia = []; end
if isempty(dia), dia = [6 2]; end
fingercode = [];
filteredImages = cell(1, length(Gfilt));
for i = 1:length(Gfilt)
    filteredImages{i} = imfilter(imOriginal, Gfilt{i}, 'replicate');
end

% Get the size of the image
[rows, cols, ~] = size(filteredImages{1});

Fmasked = zeros(rows, cols);

variances = cell(1, length(filteredImages));
for i = 1:length(filteredImages)
    variances{i} = zeros(ceil(rows/maskSize), ceil(cols/maskSize));
end

for row = 1:maskSize:rows
    for col = 1:maskSize:cols
        x1 = col;
        x2 = min(col + maskSize - 1, cols);
        y1 = row;
        y2 = min(row + maskSize - 1, rows);
        
        % Extract the patch
        
        for j = 1:numel(filteredImages) % Changed 'i' to 'j'
            image = filteredImages{j}; % Changed 'i' to 'j'
            patch = image(y1:y2, x1:x2);
            meanValue = mean(patch(:));
            stdDeviation = std(patch(:));
            variances{j}(ceil(row/maskSize), ceil(col/maskSize)) = abs(meanValue - stdDeviation);
        end
    end
end
recalculatedKernelX = ceil(core(1)/maskSize);
recalculatedKernelY = ceil(core(2)/maskSize);

minXCoordinates = recalculatedKernelX - dia(1);
maxXCoordinates = recalculatedKernelX + dia(1);
minYCoordinates = recalculatedKernelY - dia(1);
maxYCoordinates = recalculatedKernelY + dia(1);

for i = 1:length(variances)
    for row = minYCoordinates:1:maxYCoordinates
        for col = minXCoordinates:1:maxXCoordinates
            x = col;
            y = row;
            distance = sqrt((x - recalculatedKernelX)^2 + (y - recalculatedKernelY)^2);
            if distance >= dia(2) && distance <= dia(1)
                if x < 1 || y < 1 || x > size(variances{i}, 2) || y > size(variances{i}, 1)
                    fingercode(end+1) = 0;
                    continue;
                end
                fingercode(end+1) = variances{i}(y,x);
            end
        end
    end
end

fingercodeResult = fingercode;

end
