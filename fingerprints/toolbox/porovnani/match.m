function [matchingScore, nbmatch, inputmatch, dbmatch] = match(mAi1, mAi2)

% Porovnani dvou otisku pomoci parovani markantu.
%
% [matchingScore, nbmatch, inputmatch, dbmatch] = match(mAi1, mAi2)
%
%   vstupy: mAi1 - pole s markanty z databazoveho obrazku, stejne velike
%               jako originalni obrazek otisku.
%           mAi2 - pole s markanty z porovnavaneho obrazku, otisky a tedy i polohy
%               markantu musi byt co nejlepe srovnany. stejne velike jako
%               originalni obrazek otisku.
%
%   vystupy: matchingScore - ohodnoceni p�i�azen� otisk�
%            nbmatch - pocet paru markantu
%            inputmatch - pole (stejne velikosti jako vstupni obrazek) s
%               vyznacenymi markanty ve vstupnim obrazku (mAi2) sparovane s
%               markanty ze zrovnavaneho obrazku.
%            dbmatch - pole (stejne velikosti jako vstupni obrazek) s
%               vyznacenymi markanty ve vstupnim obrazku (mAi2) sparovane s
%               markanty ze zrovnavaneho obrazku.

warning off all

dbMinutiap = double(bwmorph(mAi1, 'Shrink', 'Inf'));
inputMinutiap = double(bwmorph(mAi2, 'Shrink', 'Inf'));

[x,y]=find(dbMinutiap);
for i=1:length(x)
    dbMinutiap(x(i),y(i))=mAi1(x(i),y(i));
end

[x,y]=find(inputMinutiap);
for i=1:length(x)
    inputMinutiap(x(i),y(i))=mAi2(x(i),y(i));
end


[matchingScore,~, ~,nbmatch, inputmatch, dbmatch] = score(dbMinutiap ,inputMinutiap);
 
%--------------------------------------------------------------------------
function [matchingscore, nbElementInput, nbElement, nbmatch, inputmatch, dbmatch] = score(db, input)
%Vstupy:
% db - pole, stejne velikosti jako vstupni obraz, s vyznacenymi markanty.
%   odpovida obrazu, ktery slouzi jako vzor.
% input - pole, stejne velikosti jako vstupni obraz, s vyznacenymi markanty.
%   odpovida obrazu, ktery slouzi jako porovnavany vstup.
%
%Vystupy:
% matchingScore - ohodnoceni přiřazený otisku
% nbElementInput - pocet markantu ve srovnavacim obraze
% nbelement - celkovy pocet markantu v obou vstupnich polich
% nbmatch - pocet paru markantu
% inputmatch - pole (stejne velikosti jako vstupni obrazek) s
%   vyznacenymi markanty ve vstupnim obrazku (mAi2) sparovane s
%   markanty ze zrovnavaneho obrazku.
% dbmatch - pole (stejne velikosti jako vstupni obrazek) s
%   vyznacenymi markanty ve vstupnim obrazku (mAi2) sparovane s
%   markanty ze zrovnavaneho obrazku.
nbElement = 0;
threshold =  12;
dbmatch = zeros(size(db));
inputmatch = zeros(size(input));
nbmatch = 0;
nbElementInput = 0;
nbElementDb = 0;

distance_transform = bwdist(input);

for i=1:size(input,1)
    for j=1:size(input,2)
        if input(i,j)==1
            nbElementInput = nbElementInput + 1;
            nbElement = nbElement + 1;
        end
    end
end


distot = 0
for i=1:size(db,1)
    for j=1:size(db,2)
        if db(i,j)==1
            nbElementDb = nbElementDb + 1;
            nbElement = nbElement + 1;
            distance_to_nearest_true = distance_transform(i, j);
            % disp(['Distance to the closest true value: ', num2str(distance_to_nearest_true)]);
            distot = distot + distance_to_nearest_true;
            if(distance_to_nearest_true < threshold)
                nbmatch = nbmatch + 1;
                dbmatch(i,j) = 1;
                if i <= size(input,1) && j <= size(input,2)
                    inputmatch(i,j) = 1;
                end
            end
        end
    end
end

matchingscore = nbmatch/(nbElement/2);
if matchingscore > 0.8
    disp('match');
else
    disp('Not matched');
end

fprintf('number of matched minutiae : %d\ndistance total computed : %d\n',nbmatch, distot);
fprintf('number of minutiae in input image : %d\n', nbElementInput);
fprintf('number of minutiae in database image : %d\n', nbElementDb);
fprintf('Score for minutiae : %1.2g\n', matchingscore);



