addpath ./predzpracovani
addpath ./porovnani
clear all; clc
% nacteni obrazku otisku
id = 1; num = 1;
[im1 name] = loadfprint(id, num, 'fvc02_2');
%  [imSegmented imContour] = segmentimage(im1); % pro overeni segmentace
% show_segmentation(im1, imContour);  

id = 1; num = 2;
[im2 name2] = loadfprint(id, num, 'fvc02_2');
[imSegmented imContour] = segmentimage(im2); % pro overeni segmentace
% show_segmentation(im2, imContour);   
imS1 = preprocess(im1, name, 9);

imS2 = preprocess(im2, name2, 9);

% ZAROVNANI
% a) automatickyim2Align
[mAi2Align im2Align im2SkAlign offset2 angle2 core2] = ...
       align2(imS1.imageSkeleton, imS1.singularityPoints, imS2.imageSkeleton, imS2.singularityPoints, imS2.minutiaArray, imS2.im);
figure;
imshow(im2Align);
imS2 = preprocess(im2Align, name2, 9);

% b) manualne
% [im2Align offset ang jadra] = ...
%      align_manually(imS1.im, imS2.im);
%disp(jadra)
%imS1.singularityPoints = jadra(1,:);
%imS2.singularityPoints = jadra(2,:);
% disp(imS1.singularityPoints);
% disp(imS2.singularityPoints);
% ZOBRAZENI ZAROVNANI

show_align(imS1.imageSkeleton, imS2.imageSkeleton);
show_minutia(imS1.im, imS1.imageSkeleton, imS1.minutiaArray);
show_minutia(imS2.im, imS2.imageSkeleton, imS2.minutiaArray);

% POROVNANI
% a) pomoci markatnu
% [matchingScore1, nbmatch1, inputmatch1, dbmatch1] = match(imS1.minutiaArray, imS2.minutiaArray); %markantovy align

% b) pomoci fingercodu
matchingScoreG1 = fingercode_score(imS1.im, imS1.singularityPoints, im2Align, imS2.singularityPoints); %fingercode align

%zobrazeni vystupu
disp(' ')
% disp(['Minutiae score: ' num2str(matchingScore1)]);
disp(['Score of fingercode matching: ' num2str(matchingScoreG1)]);