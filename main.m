load('keystrokes.mat');

N = length(ids);
stejne = [];
ruzne = [];
for x = 1:N
    for y=(x+1): N
        s = compareKeyStrokes(latencies(x,:), latencies(y,:));
        if (ids(x) == ids(y))
            stejne(end+1) = s;
        else
            ruzne(end+1) = s;
        end
    end
end
