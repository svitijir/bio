% iris_demo.m - preview of the iris recognition process
% E. Bakstein 21.11.2011
% based on Iris toolbox by Libor Masek, University of Australia
% initialize variables, constants etc.

% settings
iris_init


% start the encoding process:
tic

% the initial settings shows precalculated values, stored in cache!
[template mask]= createiristemplate('Images/x14.jpg');
% [template mask]= createiristemplate('Images/x2.jpg');

disp(['iris template created in ' num2str(irisHammingDistance(template, template, mask, mask)) 'distance ']);
disp(['iris template created in ' num2str(toc) 's']);