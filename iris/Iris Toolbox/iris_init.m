% iris_init.m - initialize Iris toolbox, set path, constants etc
% E. Bakstein 21.11.2011
% initialize variables, constants etc.

% settings
global irisConfig;

irisConfig.databasePath = 'Data/'; % path to image database
irisConfig.useCache = false;
irisConfig.loNoiseThreshold = 100;
irisConfig.cachePath = [irisConfig.databasePath 'cache/'];
irisConfig.diagPath = [irisConfig.databasePath 'consecutive/'];
irisConfig.showImages = 1;

addpath('Normal_encoding','Segmentation','ToDo');