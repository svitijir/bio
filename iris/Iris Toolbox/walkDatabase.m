% walkDatabase.m - load irisCode database and iterate through it

% E. Bakstein 21.11.2011

% initialize iris toolbox
iris_init;

% the database, containing calculated irisCode images
load database.mat

sides = 'LR';

[code_main, mask_main]= createiristemplate('Images/x21.jpg');

best_match = inf;
best_match_index = inf;
best_side = "L";
for subject = 1:length(database)
    
    for side = 1:2
        for image = 1:length(database(subject).(sides(side)))
            template = database(subject).(sides(side))(image).template;
            mask = database(subject).(sides(side))(image).mask;
            
            % display current progress
            
            HD = irisHammingDistance(code_main, mask_main, template, mask);
            disp([num2str(subject) ' > ' sides(side) num2str(image) "dist" HD])

            best_match = min(best_match, HD);
            if best_match == HD
                best_match_index = subject;
                best_side = sides(side);
            end

        end
    end
end

disp(['Best match: ' num2str(best_match_index) ' with distance ' num2str(best_match) "side: " best_side]);