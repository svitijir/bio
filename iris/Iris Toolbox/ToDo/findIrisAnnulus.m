% findIrisAnnulus - find circular iris boudaries in the input eye image
%
% USAGE:
%   [circlePupil, circleIris] = findIrisAnnulus(eyeimage)
%    circlePupil - inner iris edge circle parameter vector [x,y,radius]
%    circleIris - outer iris edge circle parameter vector [x,y,radius]
%
% E. Bakstein 21.11.2011
% Based on original Iris Toolbox by Libor Masek

function [circlepupil, circleiris] = findIrisAnnulus(eyeimage)
disp("separating iris");

minRadius_pupil = 25;
maxRadius_pupil = 50;

maxRadius_iris = 130;
% figure;
% imshow(eyeimage);
eyeimage_normalized = histeq(eyeimage);
% figure;
% imshow(eyeimage_normalized);
eyeimage_normalized = medfilt2(imgaussfilt(eyeimage_normalized, 3.52));
% figure;
% imshow(eyeimage_normalized);
eyeimage_normalized_pupil = medfilt2(imgaussfilt(eyeimage, 2));

edged_image = edge(eyeimage_normalized,'canny', 0.13);
edged_image2 = edge(eyeimage_normalized,'canny', 0.13);
% figure;
% imshow(edged_image, []);
% figure;
% imshow(edged_image2, []);


[pupil_center, pupil_radius] = findCircles(edged_image, [0,0], 0, minRadius_pupil, maxRadius_pupil);

[iris_center, iris_radius] = findCircles(edged_image2, pupil_center,pupil_radius*1.3, pupil_radius*1.3, maxRadius_iris);

circlepupil = [ceil(pupil_center(1)), ceil(pupil_center(2)), ceil(pupil_radius)];
circleiris = [ceil(iris_center(1)), ceil(iris_center(2)), ceil(iris_radius)];
% disp("lol done with iris");
figure;
imshow(eyeimage_normalized, []);
hold on;
viscircles(pupil_center, pupil_radius, 'Color', 'r'); % Circle 1 in red
viscircles(iris_center, iris_radius, 'Color', 'b'); % Circle 2 in blue
hold off;
end


function [center_result, radius_result] = findCircles(eyeimage, center, radius_of_inner, min_radius, max_radius)
[imgHeight, imgWidth, ~] = size(eyeimage);

[xGrid, yGrid] = meshgrid(1:imgWidth, 1:imgHeight);


% min_center = floor(center - radius_of_inner);
% max_center = ceil(center + radius_of_inner);


% Calculate distance from center for each point
distFromCenter = sqrt((xGrid - center(1,1)).^2 + (yGrid - center(1,2)).^2);
innerCircleMask = zeros(size(eyeimage));
if(radius_of_inner)
    innerCircleMask = distFromCenter <= radius_of_inner;
end



edges = eyeimage;
%edges = edges .* double(~innerCircleMask);

best_match = -1;

center_result = [-1, -1];


for i = max_radius:-1:min_radius
    radius = i;
    outerRadius = radius_of_inner + radius;
    
    outerCircleMask = distFromCenter <= outerRadius;
    annulusMask = ones(size(eyeimage));
    
    if(radius_of_inner)
        annulusMask = outerCircleMask & ~innerCircleMask;
    end
    
    edges = edges & annulusMask;
    
    H = zeros(size(edges));
    test = zeros(size(edges));
    
    for x = 1:size(edges, 2)
        for y = 1:size(edges, 1)
            if annulusMask(y,x) && edges(y, x) % For each edge pixel
                H = addCircleContour(H, x,y, radius);
                test(y,x)= 1;
            end
        end
    end
    
    peaks = houghpeaks(H', 1);
    
    if(max(H(:)) > best_match)
        best_match = max(H(:));
        center_result = peaks(1,:);
        % H = H - min(H(:));
        % H = H / max(H(:));
        radius_result = radius;
        
        % H = H - min(H(:));
        % H = H / max(H(:));
        %  figure;
        % % % % Visualize the results
        %   imsho
        % 
        % 
        % w(test);
        %   hold on;
        %   h=imagesc(H);
        %  % colormap('jet');
        
        % %  % % Set the transparency of each image to 50%
        %   alpha(h, 0.8);
        %   viscircles(peaks, radius * ones(size(peaks, 1), 1), 'EdgeColor', 'r');
    end
    
end

    function matrixWithCircle = addCircleContour(matrix, centerX, centerY, radius)
        % Get the size of the matrix
        [rows, cols] = size(matrix);
        
        % Create a grid of coordinates
        [xGrid, yGrid] = meshgrid(1:cols, 1:rows);
        
        % Calculate the distance of each point from the center
        distances = sqrt((xGrid - centerX).^2 + (yGrid - centerY).^2);
        
        % Find points that are on the contour of the circle (approximately equal to the radius)
        contourMask = abs(distances - radius) < 0.5; % Adjust this for thicker or thinner contour
        
        % Add 1 to the matrix values at the contour positions
        matrixWithCircle = matrix;
        matrixWithCircle(contourMask) = matrixWithCircle(contourMask) + 1;
    end

end