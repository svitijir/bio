% Initialize arrays for storing Hamming distances
intra_class_distances = [];
inter_class_distances = [];

% Iterate over all subjects and their images
for i = 1:length(database)
    for j = 1:2 % For both eyes
        for k = 1:length(database(i).(sides(j)))
            template1 = database(i).(sides(j))(k).template;
            mask1 = database(i).(sides(j))(k).mask;

            % Compare with every other subject and image
            for m = 1:length(database)
                for n = 1:2
                    for p = 1:length(database(m).(sides(n)))
                        template2 = database(m).(sides(n))(p).template;
                        mask2 = database(m).(sides(n))(p).mask;

                        % Calculate Hamming Distance
                        HD = irisHammingDistance(template1, mask1, template2, mask2);

                        % Classify the comparison and store the distance
                        if i == m && j == n
                            intra_class_distances = [intra_class_distances, HD];
                        else
                            inter_class_distances = [inter_class_distances, HD];
                        end
                    end
                end
            end
        end
    end
end

% Plot Histogram
figure;
histogram(intra_class_distances, 'BinWidth', 0.01, 'FaceColor', 'r');
hold on;
histogram(inter_class_distances, 'BinWidth', 0.01, 'FaceColor', 'b');
hold off;
title('Histogram of Hamming Distances');
xlabel('Hamming Distance');
ylabel('Frequency');
legend('Intra-Class', 'Inter-Class');
% Plot Histogram for Intra-Class Distances
figure; % Create a new figure window
histogram(intra_class_distances, 'BinWidth', 0.01, 'FaceColor', 'r');
title('Histogram of Intra-Class Hamming Distances');
xlabel('Hamming Distance');
ylabel('Frequency');
legend('Intra-Class');

% Plot Histogram for Inter-Class Distances
figure; % Create another new figure window
histogram(inter_class_distances, 'BinWidth', 0.01, 'FaceColor', 'b');
title('Histogram of Inter-Class Hamming Distances');
xlabel('Hamming Distance');
ylabel('Frequency');
legend('Inter-Class');

% Concatenate the intra-class and inter-class distances into a single array
all_distances = [intra_class_distances inter_class_distances];
% Create labels for the distances, 0 for intra-class and 1 for inter-class
all_labels = [zeros(size(intra_class_distances)) ones(size(inter_class_distances))];
% Define a range of thresholds to test
thresholds = linspace(min(all_distances), max(all_distances), 100);

% Initialize True Positive Rate (TPR) and False Positive Rate (FPR) arrays
TPR = zeros(1, length(thresholds));
FPR = zeros(1, length(thresholds));

% Calculate TPR and FPR for each threshold
for i = 1:length(thresholds)
    threshold = thresholds(i);
    
    % True Positives (TP): intra-class comparisons below the threshold
    TP = sum(intra_class_distances < threshold);
    
    % False Positives (FP): inter-class comparisons below the threshold
    FP = sum(inter_class_distances < threshold);
    
    % True Negatives (TN): inter-class comparisons above the threshold
    TN = sum(inter_class_distances >= threshold);
    
    % False Negatives (FN): intra-class comparisons above the threshold
    FN = sum(intra_class_distances >= threshold);
    
    % Calculate TPR and FPR
    TPR(i) = TP / (TP + FN);
    FPR(i) = FP / (FP + TN);
end

% Plot the ROC curve
figure;
plot(FPR, TPR, 'LineWidth', 2);
title('ROC Curve for Iris Recognition');
xlabel('False Positive Rate');
ylabel('True Positive Rate');
grid on;

% Find the optimal index and the corresponding threshold
[~, optimal_idx] = max(TPR - FPR);
optimal_threshold = thresholds(optimal_idx);
disp(['Optimal threshold: ' num2str(optimal_threshold)]);

thresholds = linspace(min(all_distances), max(all_distances), 100);

% Initialize False Negative Rate (FNR) and False Positive Rate (FPR) arrays
FNR = zeros(1, length(thresholds));
FPR = zeros(1, length(thresholds));

% Calculate FNR and FPR for each threshold
for i = 1:length(thresholds)
    threshold = thresholds(i);
    
    % False Negatives (FN): intra-class comparisons above the threshold
    FN = sum(intra_class_distances >= threshold);
    % True Positives (TP): intra-class comparisons below the threshold
    TP = sum(intra_class_distances < threshold);
    
    % False Positives (FP): inter-class comparisons below the threshold
    FP = sum(inter_class_distances < threshold);
    % True Negatives (TN): inter-class comparisons above the threshold
    TN = sum(inter_class_distances >= threshold);
    
    % Calculate FNR and FPR
    FNR(i) = FN / (TP + FN);
    FPR(i) = FP / (FP + TN);
end

% Plot the DET curve
figure;
plot(thresholds, FPR, 'b', 'LineWidth', 2);
hold on;
plot(thresholds, FNR, 'r', 'LineWidth', 2);
hold off;
title('DET Curve for Iris Recognition');
xlabel('Threshold');
ylabel('Rate');
legend('False Positive Rate', 'False Negative Rate');
grid on;