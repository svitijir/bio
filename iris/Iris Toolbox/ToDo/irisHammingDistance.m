% irisHammingDistance - calculate hamming distance between two iris codes
% with noise masks

% USAGE:
%   HD = irisHammingDistance(codeA,codeB, maskA, maskB) 
%   codeA, codeB - irisCodes
%   maskA, maskB - corresponding noise masks

function HD = irisHammingDistance(codeA,codeB, maskA, maskB) 

maskA = not(maskA);
maskB = not(maskB);
HD_array = inf(1,7);
% figure;
% imshow(maskA);
% figure;
% imshow(maskB);
for i = -12:1:12
    % disp(i);
    shiftedCodeA = circshift(codeA,i*12, 2);
    shiftedMaskA = circshift(maskA,i*12, 2);
    HD_array(end+1) = sum(xor(shiftedCodeA,codeB) & shiftedMaskA & maskB) / sum(shiftedMaskA & maskB);
end

HD = min(HD_array);
    
end

% ----------------------------